$(document).ready(function () {
  $(".bbest-arrow-prev img").css("opacity", "0.2");

  $(".bbest-arrow-next img").click(function () {
    $(".bbest-arrow-prev img").css("opacity", "1");
  });

  $(".valor_investir_input").click(function () {
    $(".bbest-imputs-simulador .error-text").css("display", "none");
  });

  let Count = $(".bbest-container-cards-detalhes .content").find(".card");
  if (Count.length > 4) {
    $(".bbest-cards-detalhes-produto .card")
      .addClass("card-extra-4")
      .removeClass("card");
    $(".bbest-container-cards-detalhes .content").addClass(
      "content-card-extra"
    );
    $(".bbest-container-cards-detalhes .content").removeClass(
      "novosclientesJs"
    );
    $(".bbest-cards-detalhes-produto").css("min-height", "805px");
  } else {
    $(".bbest-cards-detalhes-produto .card").removeClass("card-extra-4");
    $(".bbest-container-cards-detalhes .content").removeClass(
      "content-card-extra"
    );
    $(".bbest-container-cards-detalhes .content").removeClass(
      "novosclientesJs4"
    );
  }
  function mediaSize() {
    if (window.matchMedia("(max-width: 1090px)").matches) {
      $(".novosclientesJs").slick({
        dots: false,
        infinite: false,
        speed: 300,
        slidesToShow: 3.9,
        slidesToScroll: 1,
        arrows: false,
        variableWidth: true,
        prevArrow: $(".bbest-arrow-prev"),
        nextArrow: $(".bbest-arrow-next"),
        responsive: [
          {
            breakpoint: 1300,
            settings: {
              slidesToShow: 3.8,
              slidesToScroll: 1,
            },
          },
          {
            breakpoint: 1024,
            settings: {
              slidesToShow: 3.1,
              slidesToScroll: 1,
            },
          },
          {
            breakpoint: 991,
            settings: {
              slidesToShow: 2.7,
              slidesToScroll: 1,
            },
          },
          {
            breakpoint: 767,
            settings: {
              slidesToShow: 2.2,
              slidesToScroll: 1,
            },
          },
          {
            breakpoint: 600,
            settings: {
              slidesToShow: 1.9,
              slidesToScroll: 1,
            },
          },
          {
            breakpoint: 575,
            settings: {
              slidesToShow: 1.5,
              slidesToScroll: 1,
            },
          },
          {
            breakpoint: 450,
            settings: {
              slidesToShow: 1.3,
              slidesToScroll: 1,
            },
          },
          {
            breakpoint: 350,
            settings: {
              slidesToShow: 1.1,
              slidesToScroll: 1,
            },
          },
        ],
      });

      if (Count.length < 4) {
        $(".bbest-container-cards-detalhes .container").addClass("max_width");

        $(".bbest-slick-arrows").css("display", "flex");
      }
    }
  }

  function mediaSize2() {
    if (window.matchMedia("(max-width: 1400px)").matches) {
      $(".novosclientesJs4 .card-extra-4").addClass("card");
      $(".novosclientesJs4 .card-extra-4").removeClass("card-extra-4");

      $(".novosclientesJs4").slick({
        dots: false,
        infinite: false,
        speed: 300,
        slidesToShow: 2.1,
        slidesToScroll: 1,
        arrows: false,
        variableWidth: true,
        prevArrow: $(".bbest-arrow-prev"),
        nextArrow: $(".bbest-arrow-next"),
        responsive: [
          {
            breakpoint: 1400,
            settings: {
              slidesToShow: 2.3,
              slidesToScroll: 1,
            },
          },
          {
            breakpoint: 1300,
            settings: {
              slidesToShow: 2.5,
              slidesToScroll: 1,
            },
          },
          {
            breakpoint: 1024,
            settings: {
              slidesToShow: 2.3,
              slidesToScroll: 1,
            },
          },
          {
            breakpoint: 991,
            settings: {
              slidesToShow: 2.1,
              slidesToScroll: 1,
            },
          },
          {
            breakpoint: 850,
            settings: {
              slidesToShow: 1.7,
              slidesToScroll: 1,
            },
          },
          {
            breakpoint: 767,
            settings: {
              slidesToShow: 1.1,
              slidesToScroll: 1,
            },
          },
          {
            breakpoint: 650,
            settings: {
              slidesToShow: 1.8,
              slidesToScroll: 1,
            },
          },
          {
            breakpoint: 600,
            settings: {
              slidesToShow: 1,
              slidesToScroll: 1,
            },
          },
          {
            breakpoint: 575,
            settings: {
              slidesToShow: 1,
              slidesToScroll: 1,
            },
          },
          {
            breakpoint: 450,
            settings: {
              slidesToShow: 1,
              slidesToScroll: 1,
            },
          },
          {
            breakpoint: 350,
            settings: {
              slidesToShow: 1,
              slidesToScroll: 1,
            },
          },
        ],
      });

      if (Count.length > 4) {
        $(".bbest-cards-detalhes-produto").css("min-height", "675px");

        $(".bbest-container-cards-detalhes .container").addClass("max_width");
        $(".bbest-slick-arrows").css("display", "flex");
      }
    } else {
      if (Count.length > 4) {
        if (window.matchMedia("(max-width: 1400px)").matches) {
          $(".bbest-cards-detalhes-produto").css("min-height", "785px");
        }
      }
    }
  }

  window.addEventListener("resize", mediaSize, true);
  mediaSize();

  window.addEventListener("resize", mediaSize2, true);
  mediaSize2();

  $(".bb-jsbtnCalcular").click();
  $(window).resize(function () {
    location.reload();
  });
});